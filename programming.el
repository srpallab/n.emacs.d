(use-package json-mode
  :ensure t)

(use-package yaml-mode
  :ensure t)

(use-package restclient
  :ensure t)

(use-package cl
  :ensure t)
(use-package emmet-mode
  :ensure t
  :hook
  (sgml-mode . emmet-mode)
  (web-mode . emmet-mode)
  (css-mode . emmet-mode))

;; avoid auto completion popup, use TAB to show it  
(use-package company
  :ensure t
  :init
  (setq company-idle-delay nil
	company-minimum-prefix-length 1)
  :hook
  (after-init . global-company-mode)
  :bind
  (:map prog-mode-map
	("C-i" . company-indent-or-complete-common)
	("C-M-i" . counsel-company)))

(with-eval-after-load 'company
  (define-key company-active-map (kbd "SPC") #'company-abort))

;; web-mode
(setq web-mode-markup-indent-offset 2)
(setq web-mode-code-indent-offset 2)
(setq web-mode-css-indent-offset 2)
(use-package web-mode
  :ensure t
  :mode (("\\.js\\'" . web-mode)
	 ("\\.jsx\\'" .  web-mode)
	 ("\\.ts\\'" . web-mode)
	 ("\\.tsx\\'" . web-mode)
	 ("\\.vue\\'" . web-mode)
	 ("\\.html\\'" . web-mode))
  :commands web-mode)

;; lsp-mode
(setq lsp-log-io nil) ;; Don't log everything = speed
(setq lsp-keymap-prefix "S-l")
(setq lsp-restart 'auto-restart)
(setq lsp-ui-sideline-show-diagnostics t)
(setq lsp-ui-sideline-show-hover t)
(setq lsp-ui-sideline-show-code-actions t)

(use-package lsp-mode
  :ensure t
  :hook (
	 (web-mode . lsp-deferred)
	 (lsp-mode . lsp-enable-which-key-integration)
	 )
  :commands lsp-deferred)


(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

(defun enable-minor-mode (my-pair)
  "Enable minor mode if filename match the regexp.  MY-PAIR is a cons cell (regexp . minor-mode)."
  (if (buffer-file-name)
      (if (string-match (car my-pair) buffer-file-name)
	  (funcall (cdr my-pair)))))

(use-package prettier-js
  :ensure t)
(add-hook 'web-mode-hook #'(lambda ()
			     (enable-minor-mode
			      '("\\.jsx?\\'" . prettier-js-mode))
			     (enable-minor-mode
			      '("\\.tsx?\\'" . prettier-js-mode))))
