(tool-bar-mode -1)
(scroll-bar-mode -1)
(set-fringe-mode 10)
(delete-selection-mode t)
(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 1024 1024)) ;; 1mb

(add-hook 'org-mode-hook 'turn-on-flyspell)
(add-hook 'org-mode-hook 'turn-on-auto-fill)
(add-hook 'org-mode-hook 'electric-pair-mode)

(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(add-to-list 'org-structure-template-alist
		  '("el" "#+BEGIN_SRC emacs-lisp\n?\n#+END_SRC"))

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  ;; if nil, config is universally disabled
  (setq doom-themes-enable-bold t)
  (setq doom-themes-enable-italic t)
  (load-theme 'doom-one t)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config)
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config))

(use-package doom-modeline
  :ensure t
  :config
  (setq doom-modeline-bar-width 4)
  (setq doom-modeline-major-mode-color-icon t)
  (setq doom-modeline-minor-modes nil)
  (setq doom-modeline-window-width-limit fill-column)
  ;;   truncate-all => ~/P/F/e/l/comint.el
  (setq doom-modeline-buffer-file-name-style 'truncate-all)
  :hook (after-init . doom-modeline-mode))

;; Icons for theme
;; To install icons need to run
;; M-x all-the-icons-install-fonts
(use-package all-the-icons)

(defalias 'yes-or-no-p 'y-or-n-p)

(defun config-visit ()
  (interactive)
  (find-file "~/.emacs.d/README.org"))
(global-set-key (kbd "C-c e") 'config-visit)

(defun programming-visit ()
  (interactive)
  (find-file "~/.emacs.d/programming.org"))
(global-set-key (kbd "C-c pr") 'programming-visit)

(defun split-and-follow-horizontally ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

(defun split-and-follow-vertically ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 3") 'split-and-follow-vertically)

;; Built-in project package
(require 'project)
(global-set-key (kbd "C-x p f") #'project-find-file)

(use-package which-key
  :ensure t
  :config
  (which-key-mode))

(use-package exec-path-from-shell
  :ensure t
  :config
  (exec-path-from-shell-initialize))

(use-package expand-region
  :ensure t
  :bind (("C-=" . er/expand-region)
	 ("C--" . er/contract-region)))

(use-package counsel
  :ensure t
  :bind
  (("M-y" . counsel-yank-pop)
   :map ivy-minibuffer-map
   ("M-y" . ivy-next-line)))

(use-package ivy
  :ensure t
  :bind (("C-x b" . ivy-switch-buffer))
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "%d/%d ")
  (setq ivy-display-style 'fancy))

(use-package ivy-posframe
  :ensure t
  :init
  (ivy-posframe-mode 1)
  :config
  (setq ivy-posframe-display-functions-alist
	     '((swiper          . nil)
	       (complete-symbol . ivy-posframe-display-at-point)
	       (counsel-M-x     . ivy-posframe-display-at-frame-center)
	       (counsel-find-file . ivy-posframe-display-at-frame-center))))

(use-package swiper
     :ensure t
     :bind
     (("C-s" . swiper-isearch)
      ("C-r" . swiper-isearch)
      ("M-x" . counsel-M-x)
      ("C-x C-f" . counsel-find-file)
      ("C-c g" . counsel-git)
      ("C-c j" . counsel-git-grep))
     :config
     (progn
	(setq ivy-use-virtual-buffers t)
	(setq ivy-display-style 'fancy)
	(define-key read-expression-map (kbd "C-r") 'counsel-expression-history)))

(use-package magit
   :ensure t
   :config
   (setq git-commit-summary-max-length 50)
   :bind
   ("C-x g" . magit-status))

 (setq magit-status-margin
	  '(t "%Y-%m-%d %H:%M " magit-log-margin-width t 18))

(use-package git-gutter
  :ensure t
  :diminish (git-gutter-mode)
  :init
  (global-git-gutter-mode t)
  :config
  (custom-set-variables
   '(git-gutter:modified-sign "==") 
   '(git-gutter:added-sign "++")    
   '(git-gutter:deleted-sign "--")))

(use-package git-timemachine
  :ensure t)
